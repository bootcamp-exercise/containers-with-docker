</details>

******

<details>
<summary>Exercise 0: Clone Git repository and create your own  </summary>
 <br />


```sh
# clone repository & change into project dir
git clone git@gitlab.com:twn-devops-bootcamp/latest//07-docker/docker-exercises
cd clouexercise

# remove remote reference and add your project repository
rm -rf .git
git init 
git add .
git commit -m "Initial commit"
git remote add origin git@gitlab.com:bootcamp-exercise/containers-with-docker.git
git push -u origin master

```

</details>

******


<details>
<summary>Exercise 1: Start Mysql container  </summary>
 <br />


```sh
docker run -p 3306:3306 \
--name mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e MYSQL_DATABASE=myteamdb \
-e MYSQL_USER=admin \
-e MYSQL_PASSWORD=adminpass \
-d mysql mysqld --default-authentication-plugin=mysql_native_password

gradle build

export DB_USER=admin
export DB_PWD=adminpass 
export DB_SERVER=localhost
export DB_NAME=myteamdb

#Login to the container as admin and give privilege to admin user.
docker exec -it mysql mysql -uroot -p 

CREATE USER 'admin'@'172.17.0.1' IDENTIFIED BY 'adminpass';
GRANT ALL ON *.* TO 'admin'@'172.17.0.1';
FLUSH PRIVILEGES;

#start the application.
java -jar /build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar
```

</details>

******


<details>
<summary>Exercise 2: Start Mysql GUI container  </summary>
 <br />


```sh
# use the offical image to start phpmyadmin
docker run -p 8083:80 \
--name phpmyadmin \
--link mysql:db \
-d phpmyadmin/phpmyadmin

# access it in the browser on
localhost:8083

# login to phpmyadmin UI with either of 2 mysql user credentials:
username: admin, password:adminpass
username:root,password:root

```

</details>



******

<details>
<summary>Exercise 3: Use docker-compose for Mysql and Phpmyadmin  </summary>
 <br />


```sh
version: '3'
services:
  mysql:
    image: mysql
    ports:
      - 3306:3306
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=myteamdb
      - MYSQL_USER=admin    
      - MYSQL_PASSWORD=adminpass
    volumes:
    - mydbdata:/var/lib/mysql
    container_name: mysql
    command: --default-authentication-plugin=mysql_native_password
  phpmyadmin:
    image: phpmyadmin
    environment:
      - PMA_HOST=mysql
    ports:
      - 8083:80
    depends_on:
        - mysql  
    container_name: phpmyadmin
volumes:
  mydbdata:
    driver: local
```
```sh
#start the container
docker-compose -f docker-compose.yaml up  
```sh

</details>



******

<details>
<summary>Exercise 4:  Dockerize your Java Application  </summary>
 <br />


```sh
FROM openjdk:17-alpine3.14
RUN mkdir /opt/app-java
EXPOSE 8080
COPY build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /opt/app-java
CMD ["java", "-jar", "/opt/app-java/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]
```

</details>


******

<details>
<summary>Exercise 5: Build and push Java Application Docker Image </summary>
 <br />


```sh
#Create a docker repo on nexus, Create role(docker hosted) and user on nexus.Check the http checkbox and give 8083 as port number,
#open this port on firewall configuration on droplet.
#For token of authentication, make changes in realm , activate the docker bearer token.
#To allow http(insecure connection), Go to docker desktop-> Settings-> Docker Engine-> add these lines:
{"insecure-registries" : ["ip_address_of_docker_repo_on_nexus:8083"]}

#Login to the nexus repo
docker login ip_address_of_docker_repo_on_nexus:8083

#To build docker file: cd to the path where dockerfile is located.
docker build -t javaapp:1.0 .

#To push the image to repository:
docker push ip_address_of_docker_repo_on_nexus:8083/javaapp:1.0

```

</details>



******

<details>
<summary>Exercise 6: Add application to docker-compose  </summary>
 <br />


```sh
      version: '3'
      services:
        java-app:
          container_name: java-app
          image: {ip_address_of_docker_repo_on_nexus}/javaapp:1.0 #specify the full image name
          ports:
            - 8080:8080
          depends_on:
            - mysql
          environment:
            - DB_USER= {DB_USER}
            - DB_PWD= {DB_PWD}
            - DB_SERVER= {DB_SERVER}
            - DB_NAME= {DB_NAME}
        mysql:
          container_name: mysql1
          image: mysql:8.0
          environment:
            - MYSQL_ROOT_PASSWORD= {MYSQL_ROOT_PASSWORD}
            - MYSQL_DATABASE= {DB_NAME}
            - MYSQL_USER= {DB_USER}
            - MYSQL_PASSWORD= {DB_PWD}
          ports:
            - "3307:3306"
          volumes:
            - mydbdata:/var/lib/mysql
        phyadmin:
          image: phpmyadmin/phpmyadmin
          container_name: mypadmin1
          environment:
            - PMA_PORT= ${PMA_PORT}
            - PMA_HOST= ${PMA_HOST}
            - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
          depends_on:
            - mysql

          ports:
            - 8084:80
      volumes:
        mydbdata:local
```
```sh
docker-compose-with-app.yaml
# set all the environment variables
export DB_USER=admin
export DB_PWD=adminpass
export DB_SERVER=mysql
export DB_NAME=myteamdb

export MYSQL_ROOT_PASSWORD=rootpass

export PMA_HOST=mysql
export PMA_PORT=3306

# start all 3 containers 
docker-compose -f docker-compose.yaml up 


```

</details>

******


<details>
<summary>Exercise 7: Run application on server with docker-compose </summary>
 <br />


```sh
# on Linux server - to add an insecure docker registry, add the file /etc/docker/daemon.json with the following content
{
  "insecure-registries" : [ "{repo-address}:{repo-port}" ]
}

# restart docker for the configuration to take affect
sudo service docker restart

# check the insecure repository was added - last section "Insecure Registries:"
docker info

# do docker login to repo
docker login {repo-address}:{repo-port}

# change hardcoded HOST env var
const HOST = "{server-ip-address}";

# rebuild the application and image and push to repo
gradle build
docker build -t {repo-name}/javaapp:1.0-SNAPSHOT .
docker push {repo-name}/javaapp:1.0-SNAPSHOT 

# copy docker-compose file to remote server
scp -i ~/.ssh/id_rsa docker-compose.yaml {server-user}:{server-ip}:/home/{server-user}

# ssh into the remote server
# set all env vars as 
export DB_USER=admin
export DB_PWD=adminpass
export DB_SERVER=mysql
export DB_NAME=myteamdb

export MYSQL_ROOT_PASSWORD=root

export PMA_HOST=mysql
export PMA_PORT=3306
# run docker compose file
docker-compose -f docker-compose.yaml up 


```

</details>

******



<details>
<summary>Exercise 8: Open ports </summary>
 <br />


```sh
open port 8080 on server to access java application
```

</details>

