FROM openjdk:17-alpine3.14
RUN mkdir /opt/app-java
EXPOSE 8080
COPY build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /opt/app-java
CMD ["java", "-jar", "/opt/app-java/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]